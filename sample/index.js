import Model from './../index';
import _ from 'lodash';


class User extends Model {
    getRules() {
        return {
            name: 'required',
            email: 'email',
            address: ''
        }
    }
    getDefaults() {
        return {
            address: 'Sochi'
        }
    }
    getGetters() {
        return {
            address() {
                return 'Address: ' + this.attributes.address;
            }
        };
    }
    getSetters() {
        return {
            address(value) {
                if (_.isObject(value)) {
                    const addressArray = [];
                    _.forOwn(value, (objValue) => {
                        addressArray.push(objValue)
                    });
                    value = addressArray.join(', ');
                }
                this.attributes.address = value;
            }
        }
    }
    
}

let user = new User({
    name: 'Ivan',
    email: 'ivan@ivan.ru'
});
console.log(user.name, user.email, user.address);
/*
Ivan ivan@ivan.ru Address: Sochi
*/

user.name = 'Petya';
user.email = 'petya@petya.ru';
console.log(user.name, user.email, user.address);
/*
Petya petya@petya.ru Address: Sochi
*/

user.resetAttribute('name');
console.log(user.name, user.email, user.address);
/*
Ivan petya@petya.ru Address: Sochi
*/

user.resetAttributes();
console.log(user.name, user.email, user.address);
/*
Ivan ivan@ivan.ru Address: Sochi
*/

user.address = {
    city: 'Sochi',
    street: 'Vinogradnaya'
};
console.log(user.address);
/*
Address: Sochi, Vinogradnaya
*/

user.name = '';
user.email = 'email';
user.validate().then((result) => {
    console.log(result); // false
    console.log(user.errors);
/*
ErrorBag {
  items:
   [ { id: '1',
       field: 'name',
       msg: 'The name field is required.',
       rule: 'required',
       scope: null,
       regenerate: [Function: regenerate] },
     { id: '2',
       field: 'email',
       msg: 'The email field must be a valid email.',
       rule: 'email',
       scope: null,
       regenerate: [Function: regenerate] } ] }
*/
});

