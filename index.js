import _ from 'lodash';
import { Validator } from 'vee-validate';

export default class Model {    

    /**
     * @constructor
     * @param {Object} data - model data
     */
    constructor(data) {
        this.inputAttributes = data;
        this.fillAttributes(data);
        this.generateSettersAndGetters();
        this.validator = new Validator(this.getRules());
        this.errors = {};
    }

    /**
     * Fill model attributes
     * @param {Object} data - model data
     * @return {Void}
     */
    fillAttributes(data) {
        this.attributes = {};
        const rules = this.getRules();
        const defaults = this.getDefaults();
        _.forOwn(rules, (value, key) => {
            if (_.has(data, key)) {
                this.attributes[key] = data[key];
            } else if (_.has(defaults, key)) {
                const defaultProperty = defaults[key];
                this.attributes[key] = _.isFunction(defaultProperty) ? defaultProperty() : defaultProperty;
            } else {
                this.attributes[key] = null;
            }
        });   
    }

    /**
     * Generate attributes setters and getters
     * @return {Void}
     */
    generateSettersAndGetters() {
        if (_.isEmpty(this.attributes)) {
            return;
        }
        const rules = this.getRules();
        const getters = this.getGetters();
        const setters = this.getSetters();
        _.forOwn(rules, (value, key) => {
            let getter = () => {
                return this.attributes[key];
            };
            let setter = (value) => {
                this.attributes[key] = value;
            };
            if (_.isFunction(getters[key])) {
                getter = getters[key];
            }
            if (_.isFunction(setters[key])) {
                setter = setters[key];
            }
            if (!_.has(this, key)) {
                Object.defineProperty(this, key, {
                     get: getter,
                     set: setter
                });
            }
        });
    }

    /**
     * Validation
     * @return {Bolean}
     */
    async validate() {
        this.errors = {};
        const result = await this.validator.validateAll((this.attributes));
        this.errors = this.validator.errors;
        return result;
    }

    /**
     * Model attribute rules
     * key - name
     * value - validate rules
     * @return {Object}
     */
    getRules() {
        return {};
    }

    /**
     * Default attibutes values
     * @return {Object}
     */
    getDefaults() {
        return {};
    }

    /**
     * Custom getters for attributes
     * @return {Object}
     */
    getGetters() {
        return {};
    }

    /**
     * Custom setters for attributes
     * @return {Object}
     */
    getSetters() {
        return {};
    }

    /**
     * Discard any unsaved changes 
     * @return {Void}
     */
    resetAttributes() {
        this.fillAttributes(this.inputAttributes);
    }

    /**
     * Discards any unsaved changes to the given attribute
     * @param {String} name - attribute name
     * @return {Void}
     */
    resetAttribute(name) {
        if (_.has(this.inputAttributes, name)) {
            this.attributes[name] = this.inputAttributes[name];
        }
    }
}