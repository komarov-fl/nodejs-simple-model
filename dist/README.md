# FIN MODEL

## Introduction

> simple js model

## Code Samples

```javascript
import Model from 'fin-model'
...
class User extends Model {
    getRules() {
        return {
            name: 'required',
            email: 'email',
            address: ''
        }
    }
}
...
let user = new User({
    name: 'Ivan',
    email: 'ivan@ivan.ru'
});
```

## Installation

```javascript
npm i --save https://komarov-fl@bitbucket.org/komarov-fl/finmodel.git
```