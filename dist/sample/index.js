'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _index = require('./../index');

var _index2 = _interopRequireDefault(_index);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var User = function (_Model) {
    _inherits(User, _Model);

    function User() {
        _classCallCheck(this, User);

        return _possibleConstructorReturn(this, (User.__proto__ || Object.getPrototypeOf(User)).apply(this, arguments));
    }

    _createClass(User, [{
        key: 'getRules',
        value: function getRules() {
            return {
                name: 'required',
                email: 'email',
                address: ''
            };
        }
    }, {
        key: 'getDefaults',
        value: function getDefaults() {
            return {
                address: 'Sochi'
            };
        }
    }, {
        key: 'getGetters',
        value: function getGetters() {
            return {
                address: function address() {
                    return 'Address: ' + this.attributes.address;
                }
            };
        }
    }, {
        key: 'getSetters',
        value: function getSetters() {
            return {
                address: function address(value) {
                    if (_lodash2.default.isObject(value)) {
                        var addressArray = [];
                        _lodash2.default.forOwn(value, function (objValue) {
                            addressArray.push(objValue);
                        });
                        value = addressArray.join(', ');
                    }
                    this.attributes.address = value;
                }
            };
        }
    }]);

    return User;
}(_index2.default);

var user = new User({
    name: 'Ivan',
    email: 'ivan@ivan.ru'
});
console.log(user.name, user.email, user.address);
/*
Ivan ivan@ivan.ru Address: Sochi
*/

user.name = 'Petya';
user.email = 'petya@petya.ru';
console.log(user.name, user.email, user.address);
/*
Petya petya@petya.ru Address: Sochi
*/

user.resetAttribute('name');
console.log(user.name, user.email, user.address);
/*
Ivan petya@petya.ru Address: Sochi
*/

user.resetAttributes();
console.log(user.name, user.email, user.address);
/*
Ivan ivan@ivan.ru Address: Sochi
*/

user.address = {
    city: 'Sochi',
    street: 'Vinogradnaya'
};
console.log(user.address);
/*
Address: Sochi, Vinogradnoya
*/

user.name = '';
user.email = 'email';
user.validate().then(function (result) {
    console.log(result); // false
    console.log(user.errors);
    /*
    ErrorBag {
      items:
       [ { id: '1',
           field: 'name',
           msg: 'The name field is required.',
           rule: 'required',
           scope: null,
           regenerate: [Function: regenerate] },
         { id: '2',
           field: 'email',
           msg: 'The email field must be a valid email.',
           rule: 'email',
           scope: null,
           regenerate: [Function: regenerate] } ] }
    */
});