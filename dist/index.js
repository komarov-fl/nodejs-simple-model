'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _veeValidate = require('vee-validate');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Model = function () {

    /**
     * @constructor
     * @param {Object} data - model data
     */
    function Model(data) {
        _classCallCheck(this, Model);

        this.inputAttributes = data;
        this.fillAttributes(data);
        this.generateSettersAndGetters();
        this.validator = new _veeValidate.Validator(this.getRules());
        this.errors = {};
    }

    /**
     * Fill model attributes
     * @param {Object} data - model data
     * @return {Void}
     */


    _createClass(Model, [{
        key: 'fillAttributes',
        value: function fillAttributes(data) {
            var _this = this;

            this.attributes = {};
            var rules = this.getRules();
            var defaults = this.getDefaults();
            _lodash2.default.forOwn(rules, function (value, key) {
                if (_lodash2.default.has(data, key)) {
                    _this.attributes[key] = data[key];
                } else if (_lodash2.default.has(defaults, key)) {
                    var defaultProperty = defaults[key];
                    _this.attributes[key] = _lodash2.default.isFunction(defaultProperty) ? defaultProperty() : defaultProperty;
                } else {
                    _this.attributes[key] = null;
                }
            });
        }

        /**
         * Generate attributes setters and getters
         * @return {Void}
         */

    }, {
        key: 'generateSettersAndGetters',
        value: function generateSettersAndGetters() {
            var _this2 = this;

            if (_lodash2.default.isEmpty(this.attributes)) {
                return;
            }
            var rules = this.getRules();
            var getters = this.getGetters();
            var setters = this.getSetters();
            _lodash2.default.forOwn(rules, function (value, key) {
                var getter = function getter() {
                    return _this2.attributes[key];
                };
                var setter = function setter(value) {
                    _this2.attributes[key] = value;
                };
                if (_lodash2.default.isFunction(getters[key])) {
                    getter = getters[key];
                }
                if (_lodash2.default.isFunction(setters[key])) {
                    setter = setters[key];
                }
                if (!_lodash2.default.has(_this2, key)) {
                    Object.defineProperty(_this2, key, {
                        get: getter,
                        set: setter
                    });
                }
            });
        }

        /**
         * Validation
         * @return {Bolean}
         */

    }, {
        key: 'validate',
        value: async function validate() {
            this.errors = {};
            var result = await this.validator.validateAll(this.attributes);
            this.errors = this.validator.errors;
            return result;
        }

        /**
         * Model attribute rules
         * key - name
         * value - validate rules
         * @return {Object}
         */

    }, {
        key: 'getRules',
        value: function getRules() {
            return {};
        }

        /**
         * Default attibutes values
         * @return {Object}
         */

    }, {
        key: 'getDefaults',
        value: function getDefaults() {
            return {};
        }

        /**
         * Custom getters for attributes
         * @return {Object}
         */

    }, {
        key: 'getGetters',
        value: function getGetters() {
            return {};
        }

        /**
         * Custom setters for attributes
         * @return {Object}
         */

    }, {
        key: 'getSetters',
        value: function getSetters() {
            return {};
        }

        /**
         * Discard any unsaved changes 
         * @return {Void}
         */

    }, {
        key: 'resetAttributes',
        value: function resetAttributes() {
            this.fillAttributes(this.inputAttributes);
        }

        /**
         * Discards any unsaved changes to the given attribute
         * @param {String} name - attribute name
         * @return {Void}
         */

    }, {
        key: 'resetAttribute',
        value: function resetAttribute(name) {
            if (_lodash2.default.has(this.inputAttributes, name)) {
                this.attributes[name] = this.inputAttributes[name];
            }
        }
    }]);

    return Model;
}();

exports.default = Model;