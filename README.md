# FIN MODEL

## Introduction

> simple js model

## Code Samples

```javascript
import Model from 'fin-model'
...
class User extends Model {
    getRules() {
        return {
            name: 'required',
            email: 'email',
            address: ''
        }
    }
}
...
let user = new User({
    name: 'Ivan',
    email: 'ivan@ivan.ru'
});
```

## Installation

```
npm i --save nodejs-simple-model
```